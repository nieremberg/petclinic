FROM openjdk:16-alpine3.13

WORKDIR /app

COPY ./petclinic/.mvn/ .mvn
COPY ./petclinic/pom.xml ./
COPY ./petclinic/mvnw ./
RUN ./mvnw dependency:go-offline

COPY ./petclinic/src ./src

CMD ["./mvnw", "spring-boot:run"]


