variable "aws_region" {
  type        = string
  description = ""
  default     = "us-east-1"
}

variable "aws_profile" {
  type        = string
  description = ""
  default     = "default"
}

variable "instance_type" {
  type        = string
  description = ""
  default     = "t3.medium"
}

variable "key_name" {
  type        = string
  description = "The name for ssh key, used for aws_launch_configuration"
  default     = "aws-nieremberg"
}

variable "cluster_name" {
  type        = string
  description = "The name of AWS ECS cluster"
  default     = "petclinic"
}